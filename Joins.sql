--JOINS
--INNER
SELECT
    employee_id,
    department_name
FROM
    employees e join departments d ON e.department_id = d.department_id;

--SELF
SELECT
    e.first_name,
    m.first_name
FROM
    employees e
JOIN employees m ON e.manager_id = m.employee_id;


--NATURAL
SELECT
    *
FROM
         employees
    NATURAL JOIN departments;
--USING
SELECT
    first_name,
    department_name
FROM
    employees JOIN
DEPARTMENTS USING ( DEPARTMENT_ID );
