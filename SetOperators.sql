--SET OPERATORS
--UNION
SELECT
    first_name
FROM
    employees
UNION
SELECT
    department_name
FROM
    departments;


--UNION ALL
SELECT
    employee_id
FROM
    employees
UNION ALL
SELECT
    department_id
FROM
    departments;

--INTERSECT
SELECT
    job_id
FROM
    employees
INTERSECT
SELECT
    job_title
FROM
    jobs;

--MINUS
SELECT
    salary
FROM
    employees
MINUS
SELECT
    mın_salary
FROM
    jobs;
